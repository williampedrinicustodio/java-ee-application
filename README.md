# README #

This application is used as a way to study the technologies related to JAVA EE.

### PROJECT REQUIREMENTS ###

* Java 8
* Maven 3.3.9
* Node 4.5.0
* Npm 2.15.9
* Bower 1.8.0


### HOW TO CONFIGURE ###

1. Import the project as maven project using an IDEA
2. Execute npm install -g bower
3. Execute bower install inside of java-ee-application-war

### HOW TO RUN ###
1. Execute the command 'mvn clean install -P development'
2. Access the application by the address 'http://localhost:8080/java-ee-application/'