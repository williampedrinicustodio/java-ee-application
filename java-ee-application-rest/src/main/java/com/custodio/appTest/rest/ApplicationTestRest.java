package com.custodio.appTest.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/web-service")
public class ApplicationTestRest extends Application {
}