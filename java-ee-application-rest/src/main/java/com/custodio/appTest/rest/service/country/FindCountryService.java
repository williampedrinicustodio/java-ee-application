package com.custodio.appTest.rest.service.country;

import com.custodio.appTest.core.service.CountryService;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

/**
 * Class responsible for executing searches for the existing countries in the application.
 *
 * @author wcustodio.
 */
public class FindCountryService {

    @Inject
    private CountryService countryService;

    /**
     * Find all the exiting countries in the database.
     * @return {@link Response} with the response data.
     */
    public Response findAll() {
        return null;
    }
}