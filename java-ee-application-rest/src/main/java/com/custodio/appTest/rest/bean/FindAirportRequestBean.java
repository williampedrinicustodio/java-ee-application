package com.custodio.appTest.rest.bean;

import org.hibernate.validator.constraints.NotBlank;

import javax.ws.rs.QueryParam;

/**
 * Oject which stores the data of the request used to search for airports.
 *
 * @author wcustodio.
 */
public class FindAirportRequestBean {

    @QueryParam("countryName")
    @NotBlank(message = "app.test.countryName.blank.error")
    private String countryName;

    @QueryParam("countryCode")
    @NotBlank(message = "app.test.countryCode.blank.error")
    private String countryCode;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName( final String countryName ) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode( final String countryCode ) {
        this.countryCode = countryCode;
    }
}
