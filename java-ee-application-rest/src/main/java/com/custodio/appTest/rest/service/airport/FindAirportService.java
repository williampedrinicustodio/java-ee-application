package com.custodio.appTest.rest.service.airport;

import com.custodio.appTest.core.service.AirportService;
import com.custodio.appTest.rest.bean.FindAirportRequestBean;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

/**
 * Class responsible for executing searches for the existing airports in the application.
 *
 * @author wcustodio.
 */
public class FindAirportService {

    @Inject
    private AirportService airportService;


    /**
     * Find all the airports associated to a certain country.
     * @param request The parameters used in the request.
     * @return {@link Response} with the response data.
     */
    public Response findAllByCountryNameAndCountryCode(final FindAirportRequestBean request) {
        return null;
    }
}