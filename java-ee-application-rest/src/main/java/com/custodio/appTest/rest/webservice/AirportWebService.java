package com.custodio.appTest.rest.webservice;

import com.custodio.appTest.rest.bean.FindAirportRequestBean;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Entry point for methods related to {@link com.custodio.appTest.core.entity.Airport}.
 *
 * @author wcustodio.
 */
@Path("/airports")
public class AirportWebService {

    @GET
    public Response findAllByCriteria(@BeanParam FindAirportRequestBean request) {
        return Response.status(200).build();
    }
}