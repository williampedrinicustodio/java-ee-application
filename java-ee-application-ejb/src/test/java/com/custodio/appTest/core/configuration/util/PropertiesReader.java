package com.custodio.appTest.core.configuration.util;

import javax.ejb.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * The class used to read the properties file 'config.properties' used to configure the application.
 *
 * @author wcustodio
 */
@Singleton
public class PropertiesReader {

    private static Properties properties;

    /**
     * Map the exiting properties of the current properties file.
     */
    private static final String CSV_AIRPORT_PATH = "csv.airport.path";
    private static final String CSV_COUNTRY_PATH = "csv.country.path";
    private static final String CSV_RUNWAY_PATH = "csv.runway.path";

    /**
     * Defines a instance of {@link Properties} to obtain the properties used to configure the application.
     * @return A {@link Properties} with all data to be used to configure the application.
     * @throws IOException Error while reading the file.
     */
    public PropertiesReader() throws IOException {
        final InputStream inputStream = PropertiesReader.class.getClassLoader().getResourceAsStream("config/config.properties");
        properties = new Properties();
        properties.load(inputStream);
    }

    /**
     * Get a certain property by its name.
     * @param name The name of the property to be taken.
     * @return The value of the property.
     */
    private String getPropertyByName(final String name) {
        return properties.getProperty(name);
    }

    /**
     * Get the 'csv.airport.path' property value from the file.
     * @return The property value.
     */
    public String getCsvAirportPath() {
        return this.getPropertyByName(CSV_AIRPORT_PATH);
    }

    /**
     * Get the 'csv.country.path' property value from the file.
     * @return The property value.
     */
    public String getCsvCountryPath() {
        return this.getPropertyByName(CSV_COUNTRY_PATH);
    }

    /**
     * Get the 'csv.runway.path' property value from the file.
     * @return The property value.
     */
    public String getCsvRunwayPath() {
        return this.getPropertyByName(CSV_RUNWAY_PATH);
    }
}
