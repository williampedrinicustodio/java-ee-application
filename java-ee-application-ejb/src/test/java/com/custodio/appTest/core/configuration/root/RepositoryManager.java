package com.custodio.appTest.core.configuration.root;

import javax.enterprise.inject.Specializes;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * Specialization of the repository manager used for testing purposes.
 *
 * @author wcustodio
 */
@Specializes
public class RepositoryManager extends com.custodio.appTest.core.repository.RepositoryManager {

    @Inject
    public RepositoryManager(final EntityManager entityManager) {
        super(entityManager);
    }
}
