package com.custodio.appTest.core.configuration.root;

import com.custodio.appTest.core.service.DataBaseContentService;
import com.custodio.appTest.core.util.PropertiesReader;
import org.apache.deltaspike.core.api.provider.BeanProvider;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;

/**
 * The base test runner used for all the unit tests to configure their context.
 *
 * @author wcustodio
 */
@RunWith(CdiTestRunner.class)
public class BaseTestRunner {

    private static Boolean isDatabaseInitialized = Boolean.FALSE;

    @Inject
    protected EntityManager entityManager;

    /**
     * Begins a transaction before each unit test.
     */
    @Before
    public void beginsTransaction() throws IOException {

        this.entityManager.getTransaction().begin();

        //Creates the database content only once due to the fact that it will be used by all unit tests.
        if (!BaseTestRunner.isDatabaseInitialized) {

            // Get all the files with the content to be persisted.
            final PropertiesReader propertiesReader = BeanProvider.getContextualReference(PropertiesReader.class);
            final File countryFile = new File(BaseTestRunner.class.getResource(propertiesReader.getCsvCountryPath()).getPath());
            final File airportFile = new File(BaseTestRunner.class.getResource(propertiesReader.getCsvAirportPath()).getPath());
            final File runwayFile = new File(BaseTestRunner.class.getResource(propertiesReader.getCsvRunwayPath()).getPath());

            // Initialize the tables COUNTRY, AIRPORT and RUNWAY with the content from the CSV file.
            final DataBaseContentService dataBaseContentService = BeanProvider.getContextualReference(DataBaseContentService.class);
            dataBaseContentService.initializesContentFromCsvFile(countryFile, airportFile, runwayFile);

            BaseTestRunner.isDatabaseInitialized = Boolean.TRUE;
        }
    }

    /**
     * Commits the current transaction after the unit test.
     */
    @After
    public void endsTransaction() {
        this.entityManager.getTransaction().commit();
    }
}