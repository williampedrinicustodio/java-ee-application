package com.custodio.appTest.core.repository;

import com.custodio.appTest.core.entity.Airport;
import com.custodio.appTest.core.entity.Runway;

import javax.faces.bean.RequestScoped;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * {@link Airport} Data Access Object which contains customized queries.
 *
 * @author wcustodio.
 */
@RequestScoped
public class RunwayRepository extends BaseRepository<Runway> {

    /**
     * Find all runways associated to a certain airport.
     *
     * @param airportRef Airport identification.
     * @return List of {@link Runway} associated to a certain airport.
     */
    public List<Runway> findAllByAirportRef(final Long airportRef) {
        final TypedQuery<Runway> query = super.getRepositoryManager()
                .getEntityManager().createQuery("select r from Runway r where r.airport.id = :airportRef", Runway.class);
        query.setParameter("airportRef", airportRef);
        return query.getResultList();
    }
}