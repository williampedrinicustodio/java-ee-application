package com.custodio.appTest.core.service;


import com.custodio.appTest.core.bean.RunwayBean;
import com.custodio.appTest.core.bean.RunwayCsvBean;
import com.custodio.appTest.core.entity.Runway;
import com.custodio.appTest.core.mapper.RunwayMapper;
import com.custodio.appTest.core.repository.RunwayRepository;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Service responsible for executing procedures related to a runway.
 *
 * @author wcustodio.
 */
@Transactional
@RequestScoped
public class RunwayService {

    @Inject
    private CsvFileService csvFileService;

    @Inject
    private RunwayMapper runwayMapper;

    @Inject
    private RunwayRepository runwayRepository;

    /**
     * Read all content from a specific csv file which contains information about runways.
     * @param csv File to be read.
     * @return List of {@link Runway} with all runways from the csv file.
     * @throws IOException Error while trying to read the csv.
     */
    public List<Runway> readContentFromCsvFile(final File csv) throws IOException {
        final List<RunwayCsvBean> runwaysBean = csvFileService.read(RunwayCsvBean.class, csv);
        return runwayMapper.csvBeanToEntity(runwaysBean);
    }

    /**
     * Find all runways associated to a certain airport.
     * @param airportRef Airport identification.
     * @return List of {@link RunwayBean} associated to a certain airport.
     */
    public List<RunwayBean> findAllByAirportRef( final Long airportRef ) {
        final List<Runway> entities = runwayRepository.findAllByAirportRef(airportRef);
        final List<RunwayBean> beans = runwayMapper.entityToBean(entities);
        return beans;
    }
}
