package com.custodio.appTest.core.service;


import com.custodio.appTest.core.bean.AirportBean;
import com.custodio.appTest.core.bean.AirportCsvBean;
import com.custodio.appTest.core.entity.Airport;
import com.custodio.appTest.core.mapper.AirportMapper;
import com.custodio.appTest.core.repository.AirportRepository;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Service responsible for executing procedures related to an airport.
 *
 * @author wcustodio.
 */
@Transactional
@RequestScoped
public class AirportService {

    @Inject
    private CsvFileService csvFileService;

    @Inject
    private AirportMapper airportMapper;

    @Inject
    private AirportRepository airportRepository;

    @Inject
    private AirportRepository airportCustomRepository;

    /**
     * Read all content from a specific csv file which contains information about airports.
     * @param csv File to be read.
     * @return List of {@link AirportCsvBean} with all airports from the csv file persisted into database.
     * @throws IOException Error while trying to read the csv file.
     */
    public List<Airport> readContentFromCsvFile(final File csv) throws IOException {
        final List<AirportCsvBean> airportsBean = csvFileService.read(AirportCsvBean.class, csv);
        return airportMapper.csvBeanToEntity(airportsBean);
    }

    /**
     * Find all the airports associated to a certain country. If no parameter is passed as argument it will return all the airports from database.
     * @param countryName Country name which owns the airports
     * @param countryCode Country code which owns the airports
     * @return List of {@link AirportBean} associated to a certain country.
     */
    public List<AirportBean> findAllByCountryNameAndCountryCode( final String countryName, final String countryCode ) {
        final List<Airport> airports = airportCustomRepository.findAllByCountryNameAndCountryCode(countryName,countryCode);
        final List<AirportBean> airportsBean = airportMapper.entityToBean(airports);
        return airportsBean;
    }

    /**
     * Find a specific airport by a certain id.
     * @param id Number which identifies the airport.
     * @return {@link AirportBean} with the airport data.
     */
    public AirportBean findById( final Long id ) {
        final Airport airport = airportRepository.findById(id);
        final AirportBean airportBean = airportMapper.entityToBean(airport);
        return airportBean;
    }
}
