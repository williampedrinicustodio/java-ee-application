package com.custodio.appTest.core.repository;

import com.custodio.appTest.core.entity.Airport;

import javax.faces.bean.RequestScoped;
import javax.persistence.Query;
import java.util.List;

/**
 * {@link Airport} Data Access Object which contains customized queries.
 *
 * @author wcustodio.
 */
@RequestScoped
public class AirportRepository extends BaseRepository<Airport> {

    /**
     * Find all the airports associated to a certain country.
     *
     * @param countryName Country name which owns the airports
     * @param countryCode Country code which owns the airports
     * @return List of {@link Airport} associated to a certain country.
     */
    @SuppressWarnings("unchecked")
    public List<Airport> findAllByCountryNameAndCountryCode(final String countryName, final String countryCode) {

        final StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT A.* FROM COUNTRY C INNER JOIN AIRPORT A ON A.ISO_COUNTRY = C.CODE WHERE 1=1 ");
        if ( countryName != null && !countryName.trim().isEmpty() ) {
            queryBuilder.append("AND lower(C.NAME) LIKE lower(:countryName) ");
        }
        if ( countryCode != null && !countryCode.trim().isEmpty() ) {
            queryBuilder.append("AND lower(C.CODE) = lower(:countryCode)");
        }

        final Query query = getRepositoryManager().getEntityManager().createNativeQuery(queryBuilder.toString(), Airport.class);
        if ( countryName != null && !countryName.trim().isEmpty() ) {
            query.setParameter("countryName", "%" + countryName + "%");
        }
        if ( countryCode != null && !countryCode.trim().isEmpty() ) {
            query.setParameter("countryCode", countryCode);
        }
        return query.getResultList();
    }
}
