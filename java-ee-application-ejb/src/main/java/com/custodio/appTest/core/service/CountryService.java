package com.custodio.appTest.core.service;

import com.custodio.appTest.core.bean.CountryBean;
import com.custodio.appTest.core.bean.CountryCsvBean;
import com.custodio.appTest.core.entity.Country;
import com.custodio.appTest.core.mapper.CountryMapper;
import com.custodio.appTest.core.repository.CountryRepository;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Service responsible for executing procedures related to a country.
 *
 * @author wcustodio.
 */
@Transactional
@RequestScoped
public class CountryService {

    @Inject
    private CsvFileService csvFileService;

    @Inject
    private CountryMapper countryMapper;

    @Inject
    private CountryRepository countryRepository;

    @Inject
    private AirportService airportService;

    /**
     * Read all content from a specific csv file which contains information about countries.
     * @param csv File with the country information.
     * @return List of {@link Country} with all countries from the csv file.
     * @throws IOException Error while trying to read the csv file.
     */
    public List<Country> readContentFromCsvFile(final File csv) throws IOException {
        final List<CountryCsvBean> countriesBean = csvFileService.read(CountryCsvBean.class, csv);
        return countryMapper.csvBeanToEntity(countriesBean);
    }

    /**
     * Find all exiting countries in database.
     * @return List of {@link CountryBean}.
     */
    public List<CountryBean> findAll(){
        final List<Country> entities = this.countryRepository.findAll();
        final List<CountryBean> beans = this.countryMapper.entityToBean(entities);
        return beans;
    }
}
