package com.custodio.appTest.core.repository;

import javax.inject.Inject;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by williamcustodio on 16/09/17.
 */
public abstract class BaseRepository<T> {

    @Inject
    private RepositoryManager repositoryManager;

    /**
     * The current type of the class.
     */
    private Class<T> clazz;

    protected BaseRepository() {
        Type type = getClass().getGenericSuperclass();
        Type[] arguments = ((ParameterizedType) type).getActualTypeArguments();
        this.clazz = (Class<T>) arguments[0];
    }

    /**
     * Find a certain entity by its identifier.
     *
     * @param id The identifier of the entity.
     * @return The found entity.
     */
    public T findById(final Long id) {
        return this.repositoryManager.findById(this.clazz, id);
    }

    /**
     * Find all existing entities.
     *
     * @return The found entities.
     */
    public List<T> findAll() {
        return this.repositoryManager.findAll(this.clazz);
    }

    /**
     * Persists a certain entity in the database.
     *
     * @param entity The entity to be persisted.
     */
    public void save(final T entity) {
        this.repositoryManager.save(entity);
    }

    /**
     * Persists a certain list of entities in the database.
     *
     * @param entities The list of entities to be persisted.
     */
    public void save(final List<T> entities) {
        this.repositoryManager.save(entities);
    }

    /**
     * Update a certain entity.
     *
     * @param entity The entity to be updated.
     * @return The updated entity.
     */
    public T update(final T entity) {
        return this.repositoryManager.update(entity);
    }

    /**
     * Delete a certain entity by its identifier.
     *
     * @param id The entity identifier.
     */
    public void deleteById(final Long id) {
        this.repositoryManager.deleteById(this.clazz, id);
    }

    /**
     * Delete a certain entity.
     *
     * @param entity The entity to be deleted.
     */
    public void delete(final T entity) {
        this.repositoryManager.delete(entity);
    }

    /**
     * Get the current repository manager to be used.
     *
     * @return The {@link RepositoryManager}.
     */
    public RepositoryManager getRepositoryManager() {
        return this.repositoryManager;
    }
}
