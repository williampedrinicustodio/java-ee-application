package com.custodio.appTest.core.service;

import com.custodio.appTest.core.entity.Airport;
import com.custodio.appTest.core.entity.Country;
import com.custodio.appTest.core.entity.Runway;
import com.custodio.appTest.core.repository.CountryRepository;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service responsible for executing procedures related to database content.
 *
 * @author wcustodio.
 */
@Transactional
@RequestScoped
public class DataBaseContentService {

    @Inject
    private CountryRepository countryRepository;

    @Inject
    private CountryService countryService;

    @Inject
    private AirportService airportService;

    @Inject
    private RunwayService runwayService;

    /**
     * Read all content from a specific csv file which contains information about countries.
     *
     * @param countryFile File with the country information.
     * @param airportFile File with the airport information.
     * @param runwayFile  File with the runway information.
     * @return List of {@link Country} with all countries from the csv file.
     * @throws IOException Error while trying to read the csv file.
     */
    public void initializesContentFromCsvFile(final File countryFile, final File airportFile, final File runwayFile) throws IOException {

        final List<Country> countries = this.countryService.readContentFromCsvFile(countryFile);
        final List<Airport> airports = this.airportService.readContentFromCsvFile(airportFile);
        final List<Runway> runways = this.runwayService.readContentFromCsvFile(runwayFile);

        for (final Country country : countries) {

            //Get all the airports associated to a certain country.
            final List<Airport> countryAirports = airports.stream().filter(
                    airport -> country.getCode().equals(airport.getIsoCountry())).collect(Collectors.toList());

            //Get all the runways associated to a certain airport.
            for (final Airport airport : countryAirports) {

                final List<Runway> airportRunways = runways.stream().filter(
                        runway -> airport.getId().equals(runway.getAirportReference())).collect(Collectors.toList());

                airport.setRunways(airportRunways);
            }

            //Set the airports associated to the current country.
            country.setAirports(countryAirports);
        }

        //Persists all the data of all existing countries.
        this.countryRepository.save(countries);
    }
}
