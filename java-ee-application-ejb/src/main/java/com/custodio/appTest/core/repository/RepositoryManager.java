package com.custodio.appTest.core.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * This abstract class aims to simplify the DAO layer.
 *
 * All DAOs must inject this class.
 *
 */
public class RepositoryManager {

    /**
     * The entity manager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Instantiates a new abstract jpa dao.
     */
    public RepositoryManager() {
    }

    /**
     * Constructor used to inject a custom {@link EntityManager}
     *
     * @param entityManager The custom {@link EntityManager}.
     */
    protected RepositoryManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * Gets the entity manager.
     *
     * @return the entity manager
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }


    /**
     * Find a certain entity by its identifier.
     * @param clazz The type of the entity.
     * @param id The identifier of the entity.
     * @param <T> The Type of the entity.
     * @return The found entity.
     */
    public <T> T findById(final Class<T> clazz, final Long id) {
        return entityManager.find(clazz, id);
    }

    /**
     * Find all existing entities.
     * @param clazz The type of the entity.
     * @param <T> The Type of the entity.
     * @return The found entities.
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> findAll(final Class<T> clazz) {
        return entityManager.createQuery("from " + clazz.getName()).getResultList();
    }

    /**
     * Persists a certain entity in the database.
     * @param entity The entity to be persisted.
     * @param <T> The type of the entity.
     */
    @Transactional
    public <T> void save(final T entity) {
        entityManager.persist(entity);
        entityManager.flush();
    }

    /**
     * Persists a certain list of entities in the database.
     * @param entities The list of entities to be persisted.
     * @param <T> The type of the entity.
     */
    @Transactional
    public <T> void save(final List<T> entities) {
        for (final T entity : entities) {
            entityManager.merge(entity);
        }
        entityManager.flush();
    }

    /**
     * Update a certain entity.
     * @param entity The entity to be updated.
     * @param <T> The type of the entity to be updated.
     * @return The updated entity.
     */
    @Transactional
    public <T> T update(final T entity) {
        return entityManager.merge(entity);
    }

    /**
     * Delete a certain entity by its identifier.
     * @param clazz The class that represents the entity.
     * @param id The entity identifier.
     * @param <T> The type of the entity.
     */
    @Transactional
    public <T> void deleteById(final Class<T> clazz, final Long id) {
        final T entity = findById(clazz, id);
        this.delete(entity);
        entityManager.flush();
    }

    /**
     * Delete a certain entity.
     * @param entity The entity to be deleted.
     * @param <T> The type of the entity.
     */
    @Transactional
    public <T> void delete(final T entity) {
        if (entity != null) {
            entityManager.remove(entity);
        }
    }
}
