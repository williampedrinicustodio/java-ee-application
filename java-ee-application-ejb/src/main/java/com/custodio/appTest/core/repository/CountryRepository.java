package com.custodio.appTest.core.repository;

import com.custodio.appTest.core.entity.Country;

import javax.faces.bean.RequestScoped;

/**
 * {@link Country} Data Access Object which contains customized queries.
 *
 * @author wcustodio.
 */
@RequestScoped
public class CountryRepository extends BaseRepository<Country> {
}